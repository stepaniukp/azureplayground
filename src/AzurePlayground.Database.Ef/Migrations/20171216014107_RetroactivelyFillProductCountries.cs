﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AzurePlayground.Database.Ef.Migrations
{
    public partial class RetroactivelyFillProductCountries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [Product] 
                    SET [CountryOfOriginId] = (SELECT [CountryId] FROM [Country] WHERE [Name] = 'Poland')
                    WHERE [CategoryId] = (SELECT [CategoryId] FROM [Category] WHERE [Name] = 'Fruits');

                UPDATE [Product] 
                    SET [CountryOfOriginId] = (SELECT [CountryId] FROM [Country] WHERE [Name] = 'Russia')
                    WHERE [CategoryId] = (SELECT [CategoryId] FROM [Category] WHERE [Name] = 'Vegetables');
            ");

            migrationBuilder.DropForeignKey(
                name: "FK_Product_Country_CountryOfOriginId",
                table: "Product");

            migrationBuilder.AlterColumn<int>(
                name: "CountryOfOriginId",
                table: "Product",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Country_CountryOfOriginId",
                table: "Product",
                column: "CountryOfOriginId",
                principalTable: "Country",
                principalColumn: "CountryId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Country_CountryOfOriginId",
                table: "Product");

            migrationBuilder.AlterColumn<int>(
                name: "CountryOfOriginId",
                table: "Product",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Country_CountryOfOriginId",
                table: "Product",
                column: "CountryOfOriginId",
                principalTable: "Country",
                principalColumn: "CountryId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
