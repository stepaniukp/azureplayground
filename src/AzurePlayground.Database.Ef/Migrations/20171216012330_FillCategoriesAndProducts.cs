﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AzurePlayground.Database.Ef.Migrations
{
    public partial class FillCategoriesAndProducts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DECLARE @categoryId INT

                INSERT INTO [Category] ([Name]) VALUES ('Fruits'), ('Vegetables');

                SELECT @categoryId = [CategoryId] FROM [Category] WHERE [Name] = 'Fruits';
                INSERT INTO [Product] ([Name], [CategoryId]) 
                    VALUES ('Apple', @categoryId), ('Plum', @categoryId), ('Pear', @categoryId), ('Orange', @categoryId), ('Banana', @categoryId);

                SELECT @categoryId = [CategoryId] FROM [Category] WHERE [Name] = 'Vegetables';
                INSERT INTO [Product] ([Name], [CategoryId]) 
                    VALUES ('Onion', @categoryId), ('Carrot', @categoryId), ('Tomato', @categoryId), ('Potato', @categoryId), ('Pumpkin', @categoryId);
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DECLARE @categoryId INT

                SELECT @categoryId = [CategoryId] FROM [Category] WHERE [Name] = 'Fruits';
                DELETE FROM [Product] WHERE [CategoryId] = @categoryId;

                SELECT @categoryId = [CategoryId] FROM [Category] WHERE [Name] = 'Vegetables';
                DELETE FROM [Product] WHERE [CategoryId] = @categoryId;

                DELETE FROM [Category] WHERE [Name] IN ('Fruits', 'Vegetables')
                ");
        }
    }
}
