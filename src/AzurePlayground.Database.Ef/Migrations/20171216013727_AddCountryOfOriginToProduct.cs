﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AzurePlayground.Database.Ef.Migrations
{
    public partial class AddCountryOfOriginToProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryOfOriginId",
                table: "Product",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Product_CountryOfOriginId",
                table: "Product",
                column: "CountryOfOriginId");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Country_CountryOfOriginId",
                table: "Product",
                column: "CountryOfOriginId",
                principalTable: "Country",
                principalColumn: "CountryId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Country_CountryOfOriginId",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_CountryOfOriginId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "CountryOfOriginId",
                table: "Product");
        }
    }
}
