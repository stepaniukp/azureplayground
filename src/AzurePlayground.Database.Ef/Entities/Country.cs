﻿using System.Collections.Generic;

namespace AzurePlayground.Database.Ef.Entities
{
    public class Country
    {
        public int CountryId { get; set; }
        public string Name { get; set; }

        public List<Product> Products { get; set; }
    }
}
