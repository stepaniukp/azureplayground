﻿using System;

namespace AzurePlayground.Web.Exceptions
{
    public class ProductNotFoundException : Exception
    {
        public ProductNotFoundException(int productId)
            : base($"ID = {productId}")
        {
        }
    }
}
