﻿using System;

namespace AzurePlayground.Web.Exceptions
{
    public class ProductValidationException : Exception
    {
        public ProductValidationException(string message)
            : base(message)
        {
        }
    }
}
