﻿using AzurePlayground.Database.Ef;
using AzurePlayground.Database.Ef.Entities;
using AzurePlayground.Web.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace AzurePlayground.Web.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly PlaygroundContext _playgroundContext;

        public CategoryService(PlaygroundContext playgroundContext)
        {
            _playgroundContext = playgroundContext;
        }

        public IEnumerable<Category> GetAll()
        {
            var orderedCategories = _playgroundContext.Categories.OrderBy(x => x.Name);

            return orderedCategories.ToList();
        }
    }
}
