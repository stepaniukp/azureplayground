﻿using AzurePlayground.Database.Ef;
using AzurePlayground.Database.Ef.Entities;
using AzurePlayground.Web.Exceptions;
using AzurePlayground.Web.Services.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services
{
    public class ProductService : IProductService
    {
        private const string ProductsCacheKey = "products";
        private readonly PlaygroundContext _playgroundContext;
        private readonly ICacheService _cacheService;

        public ProductService(PlaygroundContext playgroundContext, ICacheService cacheService)
        {
            _playgroundContext = playgroundContext;
            _cacheService = cacheService;
        }

        public async Task CreateAsync(Product product)
        {
            if (product == null)
            {
                throw new ProductValidationException("No product to create was passed.");
            }

            await _playgroundContext.Products.AddAsync(product);
            await _playgroundContext.SaveChangesAsync();
            await _cacheService.InvalidateAsync(ProductsCacheKey);
        }

        public async Task DeleteAsync(int productId)
        {
            var product = await GetAsync(productId);
            _playgroundContext.Products.Remove(product);
            await _playgroundContext.SaveChangesAsync();
            await _cacheService.InvalidateAsync(ProductsCacheKey);
        }

        public async Task<Product> GetAsync(int productId)
        {
            var product = await _playgroundContext.FindAsync<Product>(productId);

            if (product == null)
            {
                throw new ProductNotFoundException(productId);
            }

            return product;
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            var products = await _cacheService.TryGetCachedAsync<IEnumerable<Product>>(ProductsCacheKey, () =>
            {
                return _playgroundContext.Products.OrderBy(x => x.ProductId).ToList();
            });

            return products;
        }

        public async Task UpdateAsync(Product product)
        {
            if (product == null)
            {
                throw new ProductValidationException("No product to update was passed.");
            }

            try
            {
                var storedProduct = await GetAsync(product.ProductId);

                storedProduct.Name = product.Name;
                storedProduct.CategoryId = product.CategoryId;
                storedProduct.CountryOfOriginId = product.CountryOfOriginId;

                await _playgroundContext.SaveChangesAsync();
                await _cacheService.InvalidateAsync(ProductsCacheKey);
            }
            catch (ProductNotFoundException)
            {
                throw new ProductValidationException("No such product exists.");
            }
        }
    }
}
