﻿using AzurePlayground.Web.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

namespace AzurePlayground.Web.Services
{
    public class CloudQueueClientFactory : ICloudQueueClientFactory
    {
        private readonly IConfiguration _configuration;

        public CloudQueueClientFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public CloudQueueClient Create()
        {
            var connectionString = _configuration["FileBlobStorage:ConnectionString"];
            var storageAccount = CloudStorageAccount.Parse(connectionString);

            return storageAccount.CreateCloudQueueClient();
        }
    }
}
