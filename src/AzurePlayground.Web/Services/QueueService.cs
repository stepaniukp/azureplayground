﻿using AzurePlayground.Web.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services
{
    public class QueueService : IQueueService
    {
        private readonly ICloudQueueClientFactory _cloudQueueClientFactory;
        private readonly IConfiguration _configuration;

        public QueueService(
            ICloudQueueClientFactory cloudQueueClientFactory,
            IConfiguration configuration)
        {
            _cloudQueueClientFactory = cloudQueueClientFactory;
            _configuration = configuration;
        }

        public async Task AddToQueueAsync(string message)
        {
            var queueClient = _cloudQueueClientFactory.Create();

            var queueName = _configuration["QueueStorage:QueueName"];
            CloudQueue queue = queueClient.GetQueueReference(queueName);

            CloudQueueMessage queueMessage = new CloudQueueMessage(message);
            await queue.AddMessageAsync(queueMessage);
        }
    }
}
