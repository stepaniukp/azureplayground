﻿using AzurePlayground.Web.Services.Contracts;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services
{
    public class ItemDocumentClientFactory : IItemDocumentClientFactory
    {
        private const string DatabaseName = "playground";
        private const string CollectionName = "Items";
    
        private readonly IConfiguration _configuration;

        public ItemDocumentClientFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<DocumentClient> CreateAsync()
        {
            var endpointUri = _configuration["DocumentDb:EndpointUri"];
            var primaryKey = _configuration["DocumentDb:PrimaryKey"];

            DocumentClient client = new DocumentClient(new Uri(endpointUri), primaryKey);

            await client.CreateDatabaseIfNotExistsAsync(new Microsoft.Azure.Documents.Database { Id = DatabaseName });
            await client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(DatabaseName), new DocumentCollection { Id = CollectionName });

            return client;
        }
    }
}
