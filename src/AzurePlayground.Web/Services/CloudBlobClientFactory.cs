﻿using AzurePlayground.Web.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace AzurePlayground.Web.Services
{
    public class CloudBlobClientFactory : ICloudBlobClientFactory
    {
        private readonly IConfiguration _configuration;

        public CloudBlobClientFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public CloudBlobClient Create()
        {
            var connectionString = _configuration["FileBlobStorage:ConnectionString"];
            var storageAccount = CloudStorageAccount.Parse(connectionString);

            return storageAccount.CreateCloudBlobClient();
        }
    }
}
