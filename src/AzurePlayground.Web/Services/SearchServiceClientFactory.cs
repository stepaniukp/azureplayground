﻿using AzurePlayground.Web.Services.Contracts;
using Microsoft.Azure.Search;
using Microsoft.Extensions.Configuration;

namespace AzurePlayground.Web.Services
{
    public class SearchServiceClientFactory : ISearchServiceClientFactory
    {
        private readonly IConfiguration _configuration;

        public SearchServiceClientFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SearchServiceClient Create()
        {
            string searchServiceName = _configuration["AzureSearch:SearchServiceName"];
            string adminApiKey = _configuration["AzureSearch:ServiceAdminApiKey"];

            SearchServiceClient serviceClient = new SearchServiceClient(searchServiceName, new SearchCredentials(adminApiKey));

            return serviceClient;
        }
    }
}
