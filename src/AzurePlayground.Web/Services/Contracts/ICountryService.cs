﻿using AzurePlayground.Database.Ef.Entities;
using System.Collections.Generic;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface ICountryService
    {
        IEnumerable<Country> GetAll();
    }
}
