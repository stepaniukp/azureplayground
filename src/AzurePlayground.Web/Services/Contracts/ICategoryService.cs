﻿using AzurePlayground.Database.Ef.Entities;
using System.Collections.Generic;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface ICategoryService
    {
        IEnumerable<Category> GetAll();
    }
}
