﻿using Microsoft.WindowsAzure.Storage.Blob;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface ICloudBlobClientFactory
    {
        CloudBlobClient Create();
    }
}
