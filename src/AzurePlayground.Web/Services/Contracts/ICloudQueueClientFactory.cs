﻿using Microsoft.WindowsAzure.Storage.Queue;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface ICloudQueueClientFactory
    {
        CloudQueueClient Create();
    }
}
