﻿using AzurePlayground.Web.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface IProductSearchService
    {
        Task<ICollection<ProductSearchResultDto>> FindProductsAsync(string query);
    }
}
