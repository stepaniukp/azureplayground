﻿using AzurePlayground.Database.Ef.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface IProductService
    {
        Task<Product> GetAsync(int productId);
        Task<IEnumerable<Product>> GetAllAsync();
        Task CreateAsync(Product product);
        Task UpdateAsync(Product product);
        Task DeleteAsync(int productId);
    }
}
