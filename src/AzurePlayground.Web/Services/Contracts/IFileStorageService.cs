﻿using AzurePlayground.Web.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface IFileStorageService
    {
        Task<FileDto> GetWithData(string blobName);
        Task<IEnumerable<FileDto>> GetAllAsync();
        Task UploadAsync(FileDto file);
        Task DeleteAsync(string blobName);
    }
}
