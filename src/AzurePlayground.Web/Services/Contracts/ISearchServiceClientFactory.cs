﻿using Microsoft.Azure.Search;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface ISearchServiceClientFactory
    {
        SearchServiceClient Create();
    }
}
