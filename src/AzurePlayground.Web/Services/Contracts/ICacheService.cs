﻿using System;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface ICacheService
    {
        bool IsCacheEnabled();
        Task<T> TryGetCachedAsync<T>(string cacheKey, Func<T> retrieve) where T : class;
        Task InvalidateAsync(string cacheKey);
    }
}
