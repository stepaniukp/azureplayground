﻿using System.Threading.Tasks;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface IQueueService
    {
        Task AddToQueueAsync(string message);
    }
}