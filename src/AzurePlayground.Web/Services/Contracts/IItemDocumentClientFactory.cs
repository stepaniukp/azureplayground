﻿using Microsoft.Azure.Documents.Client;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface IItemDocumentClientFactory
    {
        Task<DocumentClient> CreateAsync();
    }
}
