﻿using AzurePlayground.Web.Dtos;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services.Contracts
{
    public interface IItemService
    {
        Task<ItemDto> GetAsync(string id);
        Task DeleteAsync(string id);
        Task CreateOrUpdateAsync(ItemDto item);
    }
}
