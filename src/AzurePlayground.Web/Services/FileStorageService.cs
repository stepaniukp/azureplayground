﻿using AzurePlayground.Web.Dtos;
using AzurePlayground.Web.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services
{
    public class FileStorageService : IFileStorageService
    {
        private readonly ICloudBlobClientFactory _cloudBlobClientFactory;
        private readonly IConfiguration _configuration;

        public FileStorageService(
            ICloudBlobClientFactory cloudBlobClientFactory,
            IConfiguration configuration)
        {
            _cloudBlobClientFactory = cloudBlobClientFactory;
            _configuration = configuration;
        }

        public async Task DeleteAsync(string blobName)
        {
            var container = await GetContainerAsync();
            var blobReference = container.GetBlobReference(blobName);

            if (await blobReference.ExistsAsync())
            {
                await blobReference.DeleteAsync();
            }
        }

        public async Task<FileDto> GetWithData(string blobName)
        {
            var container = await GetContainerAsync();
            var blobReference = container.GetBlockBlobReference(blobName);
            byte[] data = null;

            if (await blobReference.ExistsAsync())
            {
                using (var ms = new MemoryStream())
                {
                    await blobReference.DownloadToStreamAsync(ms);
                    data = ms.ToArray();
                }
            }

            return new FileDto
            {
                Identifier = blobName,
                Data = data,
                Name = blobReference.Metadata.TryGetValue("filename", out string filename) ? filename : "unknown_file"
            };
        }

        public async Task<IEnumerable<FileDto>> GetAllAsync()
        {
            var container = await GetContainerAsync();
            BlobContinuationToken continuationToken = null;

            var result = new List<FileDto>();

            do
            {
                var response = await container.ListBlobsSegmentedAsync(string.Empty, true, BlobListingDetails.Metadata, null, continuationToken, null, null);
                continuationToken = response.ContinuationToken;

                foreach (var blob in response.Results.OfType<CloudBlockBlob>())
                {
                    var file = new FileDto
                    {
                        Identifier = blob.Name,
                        Name = blob.Metadata.TryGetValue("filename", out string filename) ? filename : "unknown_file"
                    };

                    result.Add(file);
                }
            }
            while (continuationToken != null);

            return result;
        }

        public async Task UploadAsync(FileDto file)
        {
            var container = await GetContainerAsync();

            file.Identifier = Guid.NewGuid().ToString("N");
            var blobReference = container.GetBlockBlobReference(file.Identifier);

            await blobReference.UploadFromByteArrayAsync(file.Data, 0, file.Data.Length);

            blobReference.Metadata["filename"] = file.Name;
            await blobReference.SetMetadataAsync();
        }


        private async Task<CloudBlobContainer> GetContainerAsync()
        {
            var blobClient = _cloudBlobClientFactory.Create();

            var containerName = _configuration["FileBlogStorage:ContainerName"];
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            await container.CreateIfNotExistsAsync();

            return container;
        }
    }
}
