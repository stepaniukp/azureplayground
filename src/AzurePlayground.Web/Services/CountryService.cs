﻿using AzurePlayground.Database.Ef;
using AzurePlayground.Database.Ef.Entities;
using AzurePlayground.Web.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace AzurePlayground.Web.Services
{
    public class CountryService : ICountryService
    {
        private readonly PlaygroundContext _playgroundContext;

        public CountryService(PlaygroundContext playgroundContext)
        {
            _playgroundContext = playgroundContext;
        }

        public IEnumerable<Country> GetAll()
        {
            var orderedCountries = _playgroundContext.Countries.OrderBy(x => x.Name);

            return orderedCountries.ToList();
        }
    }
}
