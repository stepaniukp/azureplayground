﻿using AzurePlayground.Web.Dtos;
using AzurePlayground.Web.Exceptions;
using AzurePlayground.Web.Services.Contracts;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services
{
    public class ItemService : IItemService
    {
        private readonly IItemDocumentClientFactory _itemDocumentClientFactory;

        public ItemService(IItemDocumentClientFactory itemDocumentClientFactory)
        {
            _itemDocumentClientFactory = itemDocumentClientFactory;
        }

        public async Task CreateOrUpdateAsync(ItemDto item)
        {
            var client = await _itemDocumentClientFactory.CreateAsync();

            try
            {
                await client.UpsertDocumentAsync(UriFactory.CreateDocumentCollectionUri("playground", "Items"), item);
            }
            catch (DocumentClientException)
            {
                throw new ItemManagementException();
            }
        }

        public async Task DeleteAsync(string id)
        {
            var client = await _itemDocumentClientFactory.CreateAsync();

            try
            {
                await client.DeleteDocumentAsync(UriFactory.CreateDocumentUri("playground", "Items", id));
            }
            catch (DocumentClientException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
            {
                throw new ItemNotFouncException();
            }
            catch (DocumentClientException)
            {
                throw new ItemManagementException();
            }
        }

        public async Task<ItemDto> GetAsync(string id)
        {
            var client = await _itemDocumentClientFactory.CreateAsync();

            FeedOptions queryOptions = new FeedOptions { MaxItemCount = 1 };

            var queryResult = client.CreateDocumentQuery<ItemDto>(
                    UriFactory.CreateDocumentCollectionUri("playground", "Items"), queryOptions)
                    .Where(i => i.Id == id)
                    .ToList();

            var item = queryResult.SingleOrDefault();

            return item;
        }
    }
}
