﻿using AzurePlayground.Web.Dtos;
using AzurePlayground.Web.Services.Contracts;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services
{
    public class ProductSearchService : IProductSearchService
    {
        private readonly ISearchServiceClientFactory _searchServiceClientFactory;

        public ProductSearchService(ISearchServiceClientFactory searchServiceClientFactory)
        {
            _searchServiceClientFactory = searchServiceClientFactory;
        }

        public async Task<ICollection<ProductSearchResultDto>> FindProductsAsync(string query)
        {
            var searchServiceClient = _searchServiceClientFactory.Create();

            var productIndexClient = searchServiceClient.Indexes.GetClient("azuresql-index");

            if (productIndexClient == null)
            {
                return Enumerable.Empty<ProductSearchResultDto>().ToList();
            }

            var parameters = new SearchParameters()
            {
                Select = new[] { "ProductId", "Name" },
                SearchFields = new [] { "Name" },
                
            };

            var searchResult = await productIndexClient.Documents.SearchAsync<ProductSearchResultDto>(query, parameters);

            return searchResult.Results
                .Select(x => x.Document)
                .ToList();
        }
    }
}
