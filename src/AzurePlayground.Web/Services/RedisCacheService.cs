﻿using AzurePlayground.Web.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Services
{
    public class RedisCacheService : ICacheService
    {
        private static string RedisConfigurationKey;

        private static readonly Lazy<ConnectionMultiplexer> LazyRedisConnection
            = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(RedisConfigurationKey));

        public static ConnectionMultiplexer RedisConnection => LazyRedisConnection.Value;

        private readonly IConfiguration _configuration;

        public RedisCacheService(IConfiguration configuration)
        {
            _configuration = configuration;
            RedisConfigurationKey = _configuration.GetValue<string>("RedisCache:ConfigurationKey");
        }

        public bool IsCacheEnabled()
        {
            return _configuration.GetValue<bool>("RedisCache:Enabled");
        }

        public async Task<T> TryGetCachedAsync<T>(string cacheKey, Func<T> retrieve) where T : class
        {
            T data = null;

            if (IsCacheEnabled())
            {
                var database = RedisConnection.GetDatabase();

                var cachedData = await database.StringGetAsync(cacheKey);

                if (!cachedData.HasValue)
                {
                    data = retrieve();
                    var serializedData = JsonConvert.SerializeObject(data);
                    await database.StringSetAsync(cacheKey, serializedData);
                }
                else
                {
                    data = JsonConvert.DeserializeObject<T>(cachedData);
                }
            }
            else
            {
                data = retrieve();
            }

            return data;
        }

        public async Task InvalidateAsync(string cacheKey)
        {
            if (cacheKey == null)
            {
                throw new ArgumentNullException(nameof(cacheKey));
            }

            if (!IsCacheEnabled())
            {
                return;
            }

            var database = RedisConnection.GetDatabase();
            await database.KeyDeleteAsync(cacheKey);
        }
    }
}
