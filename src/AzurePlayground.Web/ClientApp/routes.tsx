import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { SecretContent } from './components/SecretContent';
import { SignedIn } from './components/SignedIn';
import { Error } from './components/Error';
import { PrivateRoute } from './private-route';
import { SignOut } from './components/SignOut';
import { ProductList } from './components/ProductList';
import { ProductBackup } from './components/ProductBackup';
import { ProductCreate } from './components/ProductCreate';
import { ProductDetails } from './components/ProductDetails';
import { ProductDelete } from './components/ProductDelete';
import { FileList } from './components/FileList';
import { FileDelete } from './components/FileDelete';
import { FileDownload } from './components/FileDownload';
import { FileUpload } from './components/FileUpload';

export const routes = <Layout>
    <Route exact path='/' component={Home} />
    <Route exact path='/error' component={Error} />
    <Route path='/signedin' component={SignedIn} />
    <Route path='/signout' component={SignOut} />
    <PrivateRoute path='/secretcontent' component={SecretContent} />
    <PrivateRoute exact path='/products' component={ProductList} />
    <PrivateRoute exact path='/products/backup' component={ProductBackup} />
    <PrivateRoute exact path='/products/create' component={ProductCreate} />
    <PrivateRoute path="/products/details/:id" component={ProductDetails} />
    <PrivateRoute path="/products/delete/:id" component={ProductDelete} />
    <PrivateRoute exact path='/files' component={FileList} />
    <PrivateRoute path="/files/delete/:id" component={FileDelete} />
    <PrivateRoute path="/files/upload" component={FileUpload} />
    <PrivateRoute path="/files/download/:id" component={FileDownload} />
</Layout>;

