﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import 'isomorphic-fetch';

interface SecretContentState {
    secretData: SecretObject[];
    loading: boolean;
}

export class SecretContent extends React.Component<RouteComponentProps<{}>, SecretContentState> {
    constructor() {
        super();
        this.state = { secretData: [], loading: true };

        fetch('api/AuthorizedOnly', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
        .then(response => response.json() as Promise<SecretObject[]>)
        .then(data => {
            this.setState({ secretData: data, loading: false });
        });
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : SecretContent.renderTable(this.state.secretData);

        return <div>
            <h1>Secret content</h1>
            <p>This component is accessible only for authorized users! The data is retrieved from Web API endpoint.</p>
            {contents}
        </div>;
    }

    private static renderTable(secretData: SecretObject[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th>Key</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>
                {secretData.map(secretItem =>
                    <tr key={secretItem.key}>
                        <td>{secretItem.key}</td>
                        <td>{secretItem.value}</td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

interface SecretObject {
    key: string;
    value: number;
}
