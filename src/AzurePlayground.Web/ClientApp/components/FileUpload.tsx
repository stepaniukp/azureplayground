﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';
import axios from 'axios';

interface FileUploadState {
    file: string,
    loading: boolean
}

export class FileUpload extends React.Component<RouteComponentProps<{}>, FileUploadState> {
    constructor(props: any) {
        super(props);

        this.state = {
            file: '',
            loading: false
        };
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Data is being sent...</em></p>
            : this.renderForm();

        return <div>
            <h1>File upload</h1>
            {contents}
        </div>;
    }

    private renderForm() {
        return <form onSubmit={e => this.handleSubmit(e)}>
            <div className="form-group">
                <label htmlFor="name">File</label>
                <input type="file" className="form-control" name="file" onChange={e => this.handleChange(e)} />
            </div>
            <button type="submit" className="btn btn-primary">Upload</button>
            &nbsp;
            <Link to="/files" className="btn btn-default">
                Back
            </Link>
        </form>;
    }

    private handleChange(event: any) {
        this.setState({
            file: event.target.files[0]
        });
    }

    private handleSubmit(event: any) {
        event.preventDefault();

        this.setState({ loading: true });

        this.fileUpload(this.state.file)
            .then((response: any) => {
                if (response.status != 200) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(() => this.props.history.push('/files'))
            .catch(() => this.props.history.push('/error'));
    }

    private fileUpload(file: any) {
        const url = '/api/file/upload';
        const formData = new FormData();
        formData.append('file', file)
        const config = {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token'),
                'Content-Type': 'multipart/form-data',
            }
        }
        return axios.post(url, formData, config)
    }
}

interface ProductObject {
    productId: any,
    name: any,
    categoryId: any,
    countryOfOriginId: any;
}
