import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

export class Home extends React.Component<RouteComponentProps<{}>, {}> {
    private Tenant: string = 'pgrnd.onmicrosoft.com';
    private SignInPolicy: string = 'B2C_1_SignIn';
    private SignUpPolicy: string = 'B2C_1_SignUp';
    private ClientId: string = '4d25721f-c0d3-41b0-bcd5-8a69024177c9';
    private RedirectUri: string = window.location.protocol + '//' + window.location.host + '/signedin';

    constructor() {
        super();
    }

    public render() {
        let signedIn = sessionStorage.getItem('access_token') != null;
        let content = null;

        if (signedIn) {
            content = <div>
                <h4 className="text-muted">You're signed in.</h4>
                <br />
                <p><Link to="/SignOut">Click here</Link> to sign out.</p>
            </div>;
        }
        else {
            let signInUrl = 'https://login.microsoftonline.com/'
                + this.Tenant
                + '/oauth2/v2.0/authorize?p='
                + this.SignInPolicy
                + '&client_id='
                + this.ClientId
                + '&nonce=defaultNonce&scope=openid&response_type=id_token&prompt=login&redirect_uri='
                + this.RedirectUri;

            let signUpUrl = 'https://login.microsoftonline.com/'
                + this.Tenant
                + '/oauth2/v2.0/authorize?p='
                + this.SignUpPolicy
                + '&client_id='
                + this.ClientId
                + '&nonce=defaultNonce&scope=openid&response_type=id_token&prompt=login&redirect_uri='
                + this.RedirectUri;

            let signInAlert = null;

            if (this.props.location.state != null && this.props.location.state.signInAlert === true) {
                signInAlert = <div className="alert alert-danger">You must sign in to get access to this resource!</div>;
            }

            content = <div>
                {signInAlert}

                <h4 className="text-muted">You're not signed in.</h4>
                <br />

                <p>
                    <a href={signInUrl}>
                        Click here
                    </a> to sign in!
                </p>

                <p>
                    Don't have an account yet? <a href={signUpUrl}>
                        Sign up!
                    </a>
                </p>
            </div>
        }

        return <div>
            <h1>Welcome to Azure Playground!</h1>
            {content}
        </div>;
    }
}
