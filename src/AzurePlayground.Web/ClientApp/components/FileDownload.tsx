﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';

interface FileDownloadState {
    fileIdentifier: string;
}

interface MatchParams {
    id: string;
}

export class FileDownload extends React.Component<RouteComponentProps<MatchParams>, FileDownloadState> {
    constructor(props: any) {
        super(props);

        this.state = {
            fileIdentifier: this.props.match.params.id
        };

        fetch('api/file/getDownloadHash/' + this.state.fileIdentifier, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(response => response.json() as Promise<FileHashObject>)
            .then(data => {
                window.location.href = '/api/file/download/' + this.state.fileIdentifier + '/' + data.hash;
            })
            .catch(() => this.props.history.push('/error'));
    }

    public render() {
        return <div>
            <h1>Please wait</h1>
            <p>The file download will start in a few seconds...</p>
        </div>;
    }
}

interface FileHashObject {
    hash: string,
}

