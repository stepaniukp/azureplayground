﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';

interface CategorySearchState {
    categories: CategoryObject[];
}

export class CategorySearch extends React.Component<any, CategorySearchState> {
    constructor() {
        super();
        this.state = {
            categories: [],
        };
    }

    componentWillMount() {
        let initialCategories = [];

        fetch('api/category/getAll', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
        .then(response => response.json() as Promise<CategoryObject[]>)
        .then(data => {
            this.setState({ categories: data });
        });
    }

    render() {
        let categories = this.state.categories;
        let selectedCategoryId = this.props.selectedValue;

        let optionItems = categories.map((category) => {
            const selected = category.categoryId == selectedCategoryId;
            return <option key={category.categoryId} value={category.categoryId} selected={selected}>{category.name}</option>
        });

        return (
            <select {...this.props}>
                <option selected disabled hidden>Select category</option>
                {optionItems}
            </select>
        )
    }
}

interface CategoryObject {
    categoryId: number,
    name: string
}
