﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';

interface ProductBackupState {
    loading: boolean;
}

export class ProductBackup extends React.Component<RouteComponentProps<{}>, ProductBackupState> {
    constructor(props: any) {
        super(props);
        this.state = { loading: true };

        fetch('api/product/backup', {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(response => {
                this.setState({ loading: false });
            })
            .catch(() => this.props.history.push('/error'));
    }

    public render() {
        if (this.state.loading) {
            return <div>
                <h1>Please wait</h1>
                <p>The product backup is being created...</p>
            </div>;
        } else {
            return <div>
                <h1>Product backup has been created</h1>
                <p>Now you can return to <Link to="/products">product list</Link>.</p>
            </div>;
        }
    }
}


