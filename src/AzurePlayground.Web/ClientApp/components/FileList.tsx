﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';

interface FileListState {
    filesData: FileObject[];
    loading: boolean;
}

export class FileList extends React.Component<RouteComponentProps<{}>, FileListState> {
    constructor() {
        super();
        this.state = { filesData: [], loading: true };

        fetch('api/file/getAll', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
        .then(response => response.json() as Promise<FileObject[]>)
        .then(data => {
            this.setState({ filesData: data, loading: false });
        });
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : FileList.renderTable(this.state.filesData);

        return <div>
            <h1>Files</h1>
            <p>Here you can manage the files. Feel free to <Link to="files/upload">upload one</Link>.</p>
            {contents}
        </div>;
    }

    private static renderTable(filesData: FileObject[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {filesData.map(file =>
                    <tr key={file.identifier}>
                        <td><Link to={'files/download/' + file.identifier}>{file.name}</Link></td>
                        <td><Link to={'files/delete/' + file.identifier}>Delete</Link></td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

interface FileObject {
    identifier: string,
    name: string,
}
