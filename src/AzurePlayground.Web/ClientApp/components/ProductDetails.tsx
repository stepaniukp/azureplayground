﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';
import { CategorySearch } from './CategorySearch';
import { CountrySearch } from './CountrySearch';

interface ProductDetailsState {
    product: ProductObject;
    productId: number;
    loading: boolean;
}

interface MatchParams {
    id: number;
}

export class ProductDetails extends React.Component<RouteComponentProps<MatchParams>, ProductDetailsState> {
    constructor(props: any) {
        super(props);
        this.state = {
            productId: this.props.match.params.id,
            product: { productId: 0, name: '', categoryId: 0, countryOfOriginId: 0 },
            loading: true
        };


        fetch('api/product/get/' + this.state.productId, {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
            .then(response => response.json() as Promise<ProductObject>)
            .then(data => {
                this.setState({ product: data, loading: false });
            });
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Data is being sent...</em></p>
            : this.renderForm();

        return <div>
            <h1>Product details <small>#{this.state.productId}</small></h1>
            <p>Don't like this product? <Link to={'/products/delete/' + this.state.productId}>Delete</Link> it then.</p>
            {contents}
        </div>;
    }

    private renderForm() {
        return <form onSubmit={e => this.handleSubmit(e)}>
            <div className="form-group">
                <label htmlFor="name">Name</label>
                <input type="text" className="form-control" name="name" value={this.state.product.name} onChange={e => this.handleChange(e)} />
            </div>
            <div className="form-group">
                <label htmlFor="categoryId">Category</label>
                <CategorySearch className="form-control" name="categoryId" onChange={(e: any) => this.handleChange(e)} selectedValue={this.state.product.categoryId}></CategorySearch>
            </div>
            <div className="form-group">
                <label htmlFor="countryOfOriginId">Country of origin</label>
                <CountrySearch className="form-control" name="countryOfOriginId" onChange={(e: any) => this.handleChange(e)} selectedValue={this.state.product.countryOfOriginId}></CountrySearch>
            </div>
            <button type="submit" className="btn btn-primary">Save</button>
            &nbsp;
            <Link to="/products" className="btn btn-default">
                Back
            </Link>
        </form>;
    }

    private handleChange(event: any) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            product: { ...this.state.product, [name]: value }
        });
    }

    private handleSubmit(event: any) {
        event.preventDefault();

        this.setState({ loading: true });

        fetch('api/product/update', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            },
            body: JSON.stringify(this.state.product)
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(() => this.props.history.push('/products'))
            .catch(() => this.props.history.push('/error'));
    }
}

interface ProductObject {
    productId: any,
    name: any,
    categoryId: any,
    countryOfOriginId: any;
}
