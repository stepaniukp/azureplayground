﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';

interface ProductListState {
    productsData: ProductObject[];
    loading: boolean;
}

export class ProductList extends React.Component<RouteComponentProps<{}>, ProductListState> {
    constructor() {
        super();
        this.state = { productsData: [], loading: true };

        fetch('api/product/getAll', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
        .then(response => response.json() as Promise<ProductObject[]>)
        .then(data => {
            this.setState({ productsData: data, loading: false });
        });
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : ProductList.renderTable(this.state.productsData);

        return <div>
            <h1>Products</h1>
            <p>Here you can manage the products. Feel free to <Link to="products/create">create one</Link>.</p>
            <p>The product list is automatically backed up daily at 1:00 AM UTC. At any time, you can create <Link to="products/backup">additional backup</Link>.</p>
            {contents}
        </div>;
    }

    private static renderTable(productsData: ProductObject[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {productsData.map(secretItem =>
                    <tr key={secretItem.productId}>
                        <td>{secretItem.productId}</td>
                        <td>{secretItem.name}</td>
                        <td><Link to={'products/details/' + secretItem.productId}>View details</Link></td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

interface ProductObject {
    productId: number,
    name: string,
    categoryId: number,
    countryOfOriginId: number;
}
