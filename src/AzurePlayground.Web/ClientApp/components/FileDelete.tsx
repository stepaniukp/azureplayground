﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';

interface FileDeleteState {
    fileIdentifier: string;
}

interface MatchParams {
    id: string;
}

export class FileDelete extends React.Component<RouteComponentProps<MatchParams>, FileDeleteState> {
    constructor(props: any) {
        super(props);

        this.state = {
            fileIdentifier: this.props.match.params.id,
        };

        fetch('api/file/delete/' + this.state.fileIdentifier, {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
        .then(response => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        })
        .then(() => this.props.history.push('/files'))
        .catch(() => this.props.history.push('/error'));
    }

    public render() {
        return <div>
            <h1>Deleting file</h1>
            <p>The file is being deleted...</p>
        </div>;
    }
}

