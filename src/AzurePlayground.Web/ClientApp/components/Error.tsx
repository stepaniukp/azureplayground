﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

export class Error extends React.Component<RouteComponentProps<{}>> {
    constructor() {
        super();
    }

    public render() {
        return <div>
            <h1 className="text-danger">THIS IS ERROR!</h1>
            <p>That's sad. <Link to="/">Go home</Link>.</p>
        </div>;
    }
}
