﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';

export class SignOut extends React.Component<RouteComponentProps<{}>> {
    constructor() {
        super();
    }

    public render() {
        return <div><p>You're now signed out. Redirecting...</p></div>;
    }

    public componentDidMount() {
        sessionStorage.removeItem('access_token');
        this.props.history.push('/');
    }
}
