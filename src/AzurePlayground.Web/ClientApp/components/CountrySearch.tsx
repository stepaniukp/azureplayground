﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';

interface CountrySearchState {
    countries: CountryObject[];
}

export class CountrySearch extends React.Component<any, CountrySearchState> {
    constructor() {
        super();
        this.state = {
            countries: [],
        };
    }

    componentWillMount() {
        let initialCountries = [];

        fetch('api/country/getAll', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
        .then(response => response.json() as Promise<CountryObject[]>)
        .then(data => {
            this.setState({ countries: data });
        });
    }

    render() {
        let countries = this.state.countries;
        let selectedCountryId = this.props.selectedValue;

        let optionItems = countries.map((country) => {
            const selected = country.countryId == selectedCountryId;

            return <option key={country.countryId} value={country.countryId} selected={selected}>{country.name}</option>
        });

        return (
            <select {...this.props}>
                <option selected disabled hidden>Select country</option>
                {optionItems}
            </select>
        )
    }
}

interface CountryObject {
    countryId: number,
    name: string
}
