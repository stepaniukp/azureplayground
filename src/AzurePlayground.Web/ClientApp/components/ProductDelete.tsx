﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';

interface ProductDeleteState {
    productId: number;
}

interface MatchParams {
    id: number;
}

export class ProductDelete extends React.Component<RouteComponentProps<MatchParams>, ProductDeleteState> {
    constructor(props: any) {
        super(props);

        this.state = {
            productId: this.props.match.params.id,
        };

        fetch('api/product/delete/' + this.state.productId, {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
            }
        })
        .then(response => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        })
        .then(() => this.props.history.push('/products'))
        .catch(() => this.props.history.push('/error'));
    }

    public render() {
        return <div>
            <h1>Deleting product</h1>
            <p>The product is being deleted...</p>
        </div>;
    }
}

