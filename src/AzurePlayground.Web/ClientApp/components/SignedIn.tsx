﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';

export class SignedIn extends React.Component<RouteComponentProps<{}>> {
    constructor() {
        super();
    }

    public render() {
        return <div><p>You're now signed in. Redirecting...</p></div>;
    }

    public componentDidMount() {
        let tokenParam = '#id_token=';
        let hash = this.props.location.hash

        if (hash.indexOf(tokenParam) == 0) {
            let token = hash.replace(tokenParam, '');
            sessionStorage.setItem('access_token', token);
            this.props.history.push('/');
        } else {
            this.props.history.push('/error');
        }
    }
}
