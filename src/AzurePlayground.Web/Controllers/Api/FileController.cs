﻿using AutoMapper;
using AzurePlayground.Web.Dtos;
using AzurePlayground.Web.Services.Contracts;
using AzurePlayground.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        private readonly IFileStorageService _fileStorageService;
        private readonly IMapper _mapper;
        private readonly IMemoryCache _cache;

        public FileController(
            IFileStorageService fileStorageService,
            IMapper mapper, 
            IMemoryCache cache)
        {
            _fileStorageService = fileStorageService;
            _mapper = mapper;
            _cache = cache;
        }

        [HttpGet("getDownloadHash/{blobName}")]
        public IActionResult GetHash(string blobName)
        {
            var hash = new Guid().ToString("N");
            _cache.Set(hash, blobName, new TimeSpan(0, 15, 0));

            return Ok(new { Hash = hash });
        }

        [AllowAnonymous]
        [HttpGet("download/{blobName}/{hash}")]
        public async Task<IActionResult> Download(string blobName, string hash)
        {
            if (_cache.TryGetValue(hash, out string blobForHash))
            {
                if (blobName != blobForHash)
                {
                    return NotFound();
                }

                var file = await _fileStorageService.GetWithData(blobName);
                return File(file.Data, "application/octet-stream", file.Name);
            }

            return StatusCode(StatusCodes.Status410Gone);
        }

        [HttpGet("getAll")]
        public async Task<IActionResult> GetAll()
        {
            var files = await _fileStorageService.GetAllAsync();
            var mappedFiles = _mapper.Map<IEnumerable<FileViewModel>>(files);

            return Ok(mappedFiles);
        }

        [HttpDelete("delete/{blobName}")]
        public async Task<IActionResult> Delete(string blobName)
        {
            await _fileStorageService.DeleteAsync(blobName);

            return NoContent();
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            byte[] fileBytes = null;

            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms);
                fileBytes = ms.ToArray();
            }

            var fileDto = new FileDto
            {
                Data = fileBytes,
                Name = file.FileName
            };

            await _fileStorageService.UploadAsync(fileDto);

            return Ok(fileDto.Identifier);
        }
    }
}