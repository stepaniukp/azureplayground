﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace AzurePlayground.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AppInsightsController : Controller
    {
        [HttpGet]
        [Route("SomeObject")]
        public object GetSomeObject()
        {
            return new
            {
                PropertyA = "A-Value",
                PropertyB = 63
            };
        }

        [HttpGet]
        [Route("SomethingWithException")]
        public object GetSomethingWithException()
        {
            throw new NotSupportedException("Not this time.");
        }
    }
}