﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class MailingController : Controller
    {
        private readonly IConfiguration _configuration;

        public MailingController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("send")]
        public async Task<IActionResult> Send()
        {
            var apiKey = _configuration["Mailing:SendGridApiKey"];
            var client = new SendGridClient(apiKey);

            var msg = new SendGridMessage()
            {
                From = new EmailAddress("azure-playground@example.com", "Azure-Playground"),
                Subject = "E-mail sent from Azure-Playground!",
                PlainTextContent = "Hello world!",
                HtmlContent = "<strong>Hello world!</strong>"
            };

            msg.AddTo(new EmailAddress(_configuration["Mailing:RecipentEmail"], _configuration["Mailing:RecipentName"]));
            var response = await client.SendEmailAsync(msg);

            return Ok();
        }
    }
}
