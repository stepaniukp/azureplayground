﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AzurePlayground.Web.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AuthorizedOnlyController : Controller
    {
        private readonly IConfiguration _configuration;

        public AuthorizedOnlyController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var data =new[]
            {
                new { Key = "TestKey1", Value = "Abc"},
                new { Key = "TestKey2", Value = "Def"},
            };

            return Ok(data);
        }
    }
}