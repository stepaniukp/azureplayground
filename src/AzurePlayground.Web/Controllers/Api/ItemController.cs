﻿using AutoMapper;
using AzurePlayground.Web.Dtos;
using AzurePlayground.Web.Exceptions;
using AzurePlayground.Web.Services.Contracts;
using AzurePlayground.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class ItemController : Controller
    {
        private readonly IItemService _itemService;
        private readonly IMapper _mapper;

        public ItemController(IItemService itemService, IMapper mapper)
        {
            _itemService = itemService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]ItemViewModel item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var itemDto = _mapper.Map<ItemDto>(item);
            await _itemService.CreateOrUpdateAsync(itemDto);

            return Ok(new { item.Id });
        }

        [HttpGet]
        public async Task<IActionResult> Get(string id)
        {
            var itemDto = await _itemService.GetAsync(id);

            if (itemDto == null)
            {
                return NotFound();
            }

            var item = _mapper.Map<ItemViewModel>(itemDto);

            return Ok(item);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _itemService.DeleteAsync(id);

                return NoContent();
            }
            catch (ItemNotFouncException)
            {
                return NotFound();
            }
        }
    }
}
