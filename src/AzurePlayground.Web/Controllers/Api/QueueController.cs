﻿using AzurePlayground.Web.Services.Contracts;
using AzurePlayground.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class QueueController : Controller
    {
        private readonly IQueueService _queueService;

        public QueueController(IQueueService queueService)
        {
            _queueService = queueService;
        }

        [HttpGet("test")]
        public IActionResult Test()
        {
            return Ok();
        }

        [HttpPost("createFruit")]
        public async Task<IActionResult> CreateFruit([FromBody]QueueItemViewModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Name))
            {
                return BadRequest();
            }

            await _queueService.AddToQueueAsync(model.Name);

            return Ok();
        }
    }
}