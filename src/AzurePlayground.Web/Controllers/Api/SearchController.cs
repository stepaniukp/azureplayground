﻿using AutoMapper;
using AzurePlayground.Web.Services.Contracts;
using AzurePlayground.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        private readonly IProductSearchService _productSearchService;
        private readonly IMapper _mapper;

        public SearchController(IProductSearchService productSearchService, IMapper mapper)
        {
            _productSearchService = productSearchService;
            _mapper = mapper;
        }

        [HttpGet("products")]
        public async Task<IActionResult> Products(string query)
        {
            var searchResult = await _productSearchService.FindProductsAsync(query);
            var mappedResult = _mapper.Map<ICollection<ProductSearchResultViewModel>>(searchResult);

            return Ok(mappedResult);
        }
    }
}