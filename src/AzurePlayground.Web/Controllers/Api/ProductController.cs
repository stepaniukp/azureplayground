﻿using AutoMapper;
using AzurePlayground.Database.Ef.Entities;
using AzurePlayground.Web.Services.Contracts;
using AzurePlayground.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AzurePlayground.Web.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public ProductController(IProductService productService, IMapper mapper, IConfiguration configuration)
        {
            _productService = productService;
            _mapper = mapper;
            _configuration = configuration;
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var product = await _productService.GetAsync(id);
            var viewModel = _mapper.Map<ProductViewModel>(product);

            return Ok(viewModel);
        }

        [HttpGet("getAll")]
        public async Task<IActionResult> GetAll()
        {
            var products = await _productService.GetAllAsync();
            var viewModel = _mapper.Map<IEnumerable<ProductViewModel>>(products);

            return Ok(viewModel);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] ProductViewModel productViewModel)
        {
            var product = _mapper.Map<Product>(productViewModel);
            await _productService.CreateAsync(product);

            return NoContent();
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update([FromBody] ProductViewModel productViewModel)
        {
            var product = _mapper.Map<Product>(productViewModel);
            await _productService.UpdateAsync(product);

            return NoContent();
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _productService.DeleteAsync(id);

            return NoContent();
        }

        [HttpGet("backup")]
        public async Task<IActionResult> Backup()
        {
            using (var httpClient = new HttpClient())
            {
                await httpClient.GetAsync(_configuration["ProductBackup:TriggerUrl"]);
            }

            return NoContent();
        }
    }
}
