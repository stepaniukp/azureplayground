﻿using AutoMapper;
using AzurePlayground.Web.Services.Contracts;
using AzurePlayground.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AzurePlayground.Web.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public CategoryController(ICategoryService categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }

        [HttpGet("getAll")]
        public IActionResult GetAll()
        {
            var categories = _categoryService.GetAll();
            var viewModel = _mapper.Map<IEnumerable<CategoryViewModel>>(categories);

            return Ok(viewModel);
        }
    }
}
