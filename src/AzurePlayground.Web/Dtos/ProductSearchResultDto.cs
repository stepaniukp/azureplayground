﻿namespace AzurePlayground.Web.Dtos
{
    public class ProductSearchResultDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
    }
}
