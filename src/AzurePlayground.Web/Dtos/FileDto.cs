﻿namespace AzurePlayground.Web.Dtos
{
    public class FileDto
    {
        public string Identifier { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
    }
}
