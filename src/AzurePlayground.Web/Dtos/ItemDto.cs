﻿using Newtonsoft.Json;

namespace AzurePlayground.Web.Dtos
{
    public class ItemDto
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }
    }
}
