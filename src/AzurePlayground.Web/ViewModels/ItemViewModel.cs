﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AzurePlayground.Web.ViewModels
{
    public class ItemViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Value { get; set; }
    }
}
