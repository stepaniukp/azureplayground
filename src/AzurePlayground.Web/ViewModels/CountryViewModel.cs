﻿namespace AzurePlayground.Web.ViewModels
{
    public class CountryViewModel
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
    }
}
