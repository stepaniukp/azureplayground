﻿namespace AzurePlayground.Web.ViewModels
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int CountryOfOriginId { get; set; }
    }
}
