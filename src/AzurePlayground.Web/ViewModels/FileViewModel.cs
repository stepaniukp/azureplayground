﻿namespace AzurePlayground.Web.ViewModels
{
    public class FileViewModel
    {
        public string Identifier { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
    }
}
