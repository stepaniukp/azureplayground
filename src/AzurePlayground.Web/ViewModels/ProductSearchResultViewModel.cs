﻿namespace AzurePlayground.Web.ViewModels
{
    public class ProductSearchResultViewModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
    }
}
