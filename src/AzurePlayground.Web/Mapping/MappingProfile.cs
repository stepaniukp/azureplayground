﻿using AutoMapper;
using AzurePlayground.Database.Ef.Entities;
using AzurePlayground.Web.Dtos;
using AzurePlayground.Web.ViewModels;

namespace AzurePlayground.Web.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Country, CountryViewModel>();
            CreateMap<FileDto, FileViewModel>();
            CreateMap<Category, CategoryViewModel>();
            CreateMap<ItemDto, ItemViewModel>();
            CreateMap<ItemViewModel, ItemDto>();

            CreateMap<Product, ProductViewModel>()
                .ForMember(dst => dst.CategoryId, m => m.MapFrom(src => src.CategoryId))
                .ForMember(dst => dst.CountryOfOriginId, m => m.MapFrom(src => src.CountryOfOriginId))
                .ForMember(dst => dst.Name, m => m.MapFrom(src => src.Name))
                .ForMember(dst => dst.ProductId, m => m.MapFrom(src => src.ProductId));

            CreateMap<ProductViewModel, Product>()
                .ForMember(dst => dst.CategoryId, m => m.MapFrom(src => src.CategoryId))
                .ForMember(dst => dst.CountryOfOriginId, m => m.MapFrom(src => src.CountryOfOriginId))
                .ForMember(dst => dst.Name, m => m.MapFrom(src => src.Name))
                .ForMember(dst => dst.ProductId, m => m.MapFrom(src => src.ProductId));

            CreateMap<ProductSearchResultDto, ProductSearchResultViewModel>();
        }
    }
}
