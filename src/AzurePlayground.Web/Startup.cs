﻿using AutoMapper;
using AzurePlayground.Database.Ef;
using AzurePlayground.Web.ErrorHandling;
using AzurePlayground.Web.Services;
using AzurePlayground.Web.Services.Contracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AzurePlayground.Web
{
    public class Startup
    {
        private IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddAuthentication(options =>
                {
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(jwtOptions =>
                {
                    jwtOptions.Authority = $"https://login.microsoftonline.com/tfp/{_configuration["AzureAdB2C:Tenant"]}/{_configuration["AzureAdB2C:SignInPolicy"]}/v2.0/";
                    jwtOptions.Audience = _configuration["AzureAdB2C:ClientId"];
                });

            services.AddDbContext<PlaygroundContext>(options
                => options.UseSqlServer(_configuration.GetConnectionString("PlaygroundDatabase")));

            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<IFileStorageService, FileStorageService>();
            services.AddTransient<ICloudBlobClientFactory, CloudBlobClientFactory>();
            services.AddTransient<IItemService, ItemService>();
            services.AddTransient<IItemDocumentClientFactory, ItemDocumentClientFactory>();
            services.AddTransient<ICloudQueueClientFactory, CloudQueueClientFactory>();
            services.AddTransient<IQueueService, QueueService>();
            services.AddTransient<IProductSearchService, ProductSearchService>();
            services.AddTransient<ISearchServiceClientFactory, SearchServiceClientFactory>();
            services.AddTransient<ICacheService, RedisCacheService>();

            services.AddAutoMapper();

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            TelemetryConfiguration.Active.InstrumentationKey = _configuration["ApplicationInsights:InstrumentationKey"];

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });

        }
    }
}
