﻿using AzurePlayground.ServiceFabric.Common;
using AzurePlayground.Stateless.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using System;
using System.Diagnostics;
using System.Fabric;
using System.Threading.Tasks;

namespace AzurePlayground.Stateless.Controllers
{
    public class HomeController : Controller
    {
        private readonly FabricClient _fabricClient;

        public HomeController(FabricClient fabricClient)
        {
            _fabricClient = fabricClient;
        }

        public async Task<IActionResult> Index()
        {
            var serviceClient = ServiceProxy.Create<IPlaygroundService>(new Uri("fabric:/AzurePlayground.ServiceFabric/AzurePlayground.Stateful"), new ServicePartitionKey(1));
            var result = await serviceClient.GetCount();

            ViewBag.Count = result;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
