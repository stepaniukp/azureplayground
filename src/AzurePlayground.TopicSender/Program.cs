﻿using Microsoft.ServiceBus.Messaging;
using System;

namespace AzurePlayground.TopicSender
{
    class Program
    {
        static void Main(string[] args)
        {
            const string topicName = "playground-topic";
            MessagingFactory factory = MessagingFactory.Create();
            TopicClient topic = factory.CreateTopicClient(topicName);

            Console.WriteLine("Enter message:");
            var message = Console.ReadLine();
            topic.Send(new BrokeredMessage(message));
        }
    }
}
