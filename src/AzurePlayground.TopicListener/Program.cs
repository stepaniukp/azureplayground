﻿using Microsoft.ServiceBus.Messaging;
using System;

namespace AzurePlayground.TopicListener
{
    class Program
    {
        static void Main(string[] args)
        {
            const string topicName = "playground-topic";
            const string subscriptionName = "playground-subscription";

            MessagingFactory factory = MessagingFactory.Create();
            SubscriptionClient subscription = factory.CreateSubscriptionClient(topicName, subscriptionName);

            while (true)
            {
                BrokeredMessage message = subscription.Receive();

                if (message != null)
                {
                    try
                    {
                        Console.WriteLine("MessageId {0}", message.MessageId);
                        Console.WriteLine("Delivery {0}", message.DeliveryCount);
                        Console.WriteLine("Size {0}", message.Size);
                        Console.WriteLine(message.GetBody<string>());
                        message.Complete();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        message.Abandon();
                    }
                }
            }
        }
    }
}