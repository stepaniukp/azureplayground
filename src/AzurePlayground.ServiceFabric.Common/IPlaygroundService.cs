﻿using Microsoft.ServiceFabric.Services.Remoting;
using System.Threading.Tasks;

namespace AzurePlayground.ServiceFabric.Common
{
    public interface IPlaygroundService : IService
    {
        Task<long> GetCount();
    }
}
