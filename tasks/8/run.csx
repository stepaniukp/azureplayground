#r "Microsoft.WindowsAzure.Storage"
#r "System.Configuration"
#r "System.Data"

using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

class Product
{
    public int ProductId { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public int CountryOfOriginId { get; set; }

    public override string ToString() 
    {
        return $"\"{ProductId}\";\"{Name}\";\"{CategoryId}\";\"{CountryOfOriginId}\";";
    }
}

public static void Run(TimerInfo myTimer, TraceWriter log, CloudBlockBlob outputBlob)
{
    var str = ConfigurationManager.ConnectionStrings["sql_connection"].ConnectionString;
    var products = new List<Product>();

    using (SqlConnection conn = new SqlConnection(str))
    {
        conn.Open();

        using (SqlCommand command = new SqlCommand("SELECT * FROM [Product]", conn))
        {
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                var product = new Product
                {
                    ProductId = (int)reader["ProductId"],
                    Name = (string)reader["Name"],
                    CategoryId = (int)reader["CategoryId"],
                    CountryOfOriginId = (int)reader["CountryOfOriginId"]
                };
                
                products.Add(product);
            }
        }
    }

    var csvBuilder = new StringBuilder();

    foreach (var product in products)
    {
        csvBuilder.AppendLine(product.ToString());
    }

    using (var memoryStream = new MemoryStream())
    using (var writer = new StreamWriter(memoryStream))
    {
        writer.Write(csvBuilder.ToString());
        writer.Flush();
        memoryStream.Position = 0;

        var utcNow = DateTime.UtcNow;
        var dateTimeString = utcNow.ToString("yyyyMMddHHmm");

        outputBlob.Metadata["filename"] = $"Products-{dateTimeString}.csv";
        outputBlob.UploadFromStream(memoryStream);
    }
}
