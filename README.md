# README #

AzurePlayground - a pet project created for Azure learning purposes, inspired by The Accountants guild!

### What is this project for? ###

The project's based on ASP.NET Core and Azure related technology stack, possibly implemented using the new tools which might be learnt.
It uses extremally simplified monolithic architecture because the valid app's architecture isn't the aim of this project.

### Azure features used so far: ###
* Web App
* Key Vault (with Managed service identity support)
* SQL Server
* Continous Delivery ([Build definition](tasks/4/build_definition.png), [Release definition](tasks/4/release_definition.png))
* Application Insights
* Azure Active Directory B2C (with local e-mail accounts)
* Blob storage
* Azure Functions - time and http hook triggered ([C# script](tasks/8/run.csx), [JSON configuration](tasks/8/function.json))
* Cosmos DB (no longer available in demo app instance due to the service costs; see the ItemController class)
* Azure Queue Storage - QueueController is used to add items to queue which are then handled by queue-trigerred Azure Function - no front-end this time
* Azure Search - see the SearchController for this - no front-end available
* Azure Redis Cache - see the ProductService class
* SendGrid integration - see MailingController class
* Service Fabric - stateless service with web interface, calling stateful service - works only locally
* ServiceBus topics - sender and listener apps

### Live demo ###
It's supposed to be here:
[https://azure-playground.azurewebsites.net/](https://azure-playground.azurewebsites.net/)